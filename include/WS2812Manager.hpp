#include<Freenove_WS2812_Lib_for_ESP32.h>

class WS2812Manager{
    private:
    Freenove_ESP32_WS2812* strip;
    int _ledCount;
    void RGBCycleAll(int tempo);
    void setAllColor(int R, int G, int B);
    void BreatheOutAllColor(int R, int G, int B, int tempo);
    void BreatheInAllColor(int R, int G, int B, int tempo);
    void BreatheAllColor(int R, int G, int B, int tempo);
    public:
    WS2812Manager(int ledPin, int ledCount);
    static void RunRGB(void *classInstance);
};