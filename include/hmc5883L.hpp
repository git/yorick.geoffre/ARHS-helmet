#include "I2CManagement.hpp"
#define X 0
#define Y 1
#define Z 2

class Hmc5883L : I2CDevice{
    protected:
        int16_t values[3] = {0,0,0};
        double valuesScaled[3] = {0,0,0};
        double radius = 0;
        double azimuth = 0;
        double elevation = 0;
        double heading = 0;
        void readRawValues();
    public:
        Hmc5883L(int readAddress, int writeAddress);
        void beginUpdateTask();
        void update();
        static void beginUpdateTaskWrapper(void *classInstance);
        bool checkAlive() override {return true;}
        String getRawValuesWithSeparator();
        String getHeadingValue(){return String(heading);}
        String getDebugValues();
};