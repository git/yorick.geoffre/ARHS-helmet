#include "WS2812Manager.hpp"
#include "PinDefinitions.hpp"

WS2812Manager::WS2812Manager(int ledPin, int ledCount){
    strip = new Freenove_ESP32_WS2812(ledCount,ledPin);
        _ledCount = ledCount;
        if(!strip->begin())
            LOG_ERROR("WS2812Manager() error");
    }

void WS2812Manager::RGBCycleAll(int tempo){
  strip->setBrightness(255);
  for (int j = 0; j < 255; j++) {   //cycle to blue
    for (int i = 0; i < _ledCount; i++) {
      strip->setLedColorData(i, strip->Wheel((i * 256 / _ledCount + j) & 255));
    }
    LOG_VERBOSE("RGBCycleAll to blue");
    strip->show();
    vTaskDelay(tempo/ portTICK_RATE_MS);
  }  
    for (int j = 255; j > 0; j--) { //cycle back to red
    for (int i = 0; i < _ledCount; i++) {
      strip->setLedColorData(i, strip->Wheel((i * 256 / _ledCount + j) & 255));
    }
    LOG_VERBOSE("RGBCycleAll to red");
    strip->show();
    vTaskDelay(tempo/ portTICK_RATE_MS);
  }  
}

void WS2812Manager::setAllColor(int R, int G, int B){
    LOG_VERBOSE(String("setAllColor ")+String(R)+String(G)+String(B));
    esp_err_t error = strip->setAllLedsColor(R,G,B);
    if(error != 0){
        LOG_VERBOSE("setAllColor ERROR "+String(error));
    }
    //strip->show();
}

void WS2812Manager::BreatheInAllColor(int R, int G, int B, int tempo){
    LOG_VERBOSE("BreatheInAllColor");
    LOG_VERBOSE("BreatheInAllColor set color done");
    setAllColor(R,G,B);
    for(int i = 0; i < 255; i++){
        strip->setBrightness(i);
        LOG_VERBOSE("BreatheInAllColor " + String(i)
        +' '+String(R) + ":" +String(G) + ":"+ String(B));
        vTaskDelay(tempo/ portTICK_RATE_MS);
    }
}

void WS2812Manager::BreatheOutAllColor(int R, int G, int B, int tempo){
    LOG_INFO("BreatheOutAllColor");
    LOG_INFO("BreatheOutAllColor set color done");
    for(int i = 254; i > 0; i--){
        setAllColor(R,G,B);
        strip->setBrightness(i);
        LOG_INFO("BreatheOutAllColor " + String(i)
        +' '+String(R) + ":" +String(G) + ":"+ String(B));
        vTaskDelay(tempo/ portTICK_RATE_MS);
    }
}

void WS2812Manager::BreatheAllColor(int R, int G, int B, int tempo){
    BreatheInAllColor(R,B,G,tempo);
    BreatheOutAllColor(R,G,B,tempo);
}

void WS2812Manager::RunRGB(void *classInstance){
  // Get a pointer to the object instance
  WS2812Manager *obj = static_cast<WS2812Manager *>(classInstance);
  while(true){
    obj->RGBCycleAll(10);
  }
}