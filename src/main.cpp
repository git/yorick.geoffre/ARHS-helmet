#include "main.hpp"
#include "PinDefinitions.hpp"
#include "hmc5883L.hpp"

#if !defined( ARDUINO_ESP32C3_DEV )
	#error This code is intended to run on the ESP32_C3 platform! Please check your Tools->Board setting.
#endif

HellMetServoManager* hm;
WS2812Manager* LedManager;
Hmc5883L* magnetometer;
SemaphoreHandle_t serial_mutex = NULL;

void rtosLoop(void *pvParameter){
  LOG_VERBOSE("rtosloop");

  if (Serial.available()){
    String str = Serial.readString();
    LOG_INFO(str);
    switch(str[0]){
      case 's': //servo
        switch(str[1]){
          case 't': //toggle All
            if(!hm->in_use)
                xTaskCreate(HellMetServoManager::ToggleAllWrapper, "Servo Thread", 4096, hm, 6, NULL);
          break;
          default:
          break;
        }
      break;
      case 'l': //leds
      break;
      case 'm' :  //magnetometer
        switch(str[1]){
          case 'r': //read heading
            Serial.println(magnetometer->getHeadingValue());
          break;
          case 'd': //read debug
            Serial.println(magnetometer->getDebugValues());
          break;
          default:
          break;
        }
      default:
      break;
    }
    LOG_INFO("rtosloop - serial present");
    Serial.flush();
    }
  LOG_VERBOSE("rtosloop - out");
}

void setup() {
  serial_mutex = xSemaphoreCreateBinary();
  Serial.begin(9600);
  I2CManager::Setup(GPIO_NUM_10, GPIO_NUM_9);
  Serial.println("Are you alive?");
  while (!Serial.available());
  I2CManager::scanAll();
  magnetometer = new Hmc5883L(0x1E,0x1E);
  LOG_INFO("Creating mag thread");
  xTaskCreate(Hmc5883L::beginUpdateTaskWrapper, "magnetometer thread", 4096, magnetometer, 7, NULL);
  LOG_INFO("Done creating mag thread");
  Serial.println("Are you alive?");
  LedManager = new WS2812Manager(GPIO_NUM_5,1);
  xTaskCreate(WS2812Manager::RunRGB, "RGB thread", 4096, LedManager, 6, NULL);
  delay(100);
  hm = new HellMetServoManager();
  xTaskCreate(HellMetServoManager::ToggleAllWrapper, "Servo Thread", 4096, hm, 6, NULL);
}

void loop(){
  rtosLoop(NULL);
}