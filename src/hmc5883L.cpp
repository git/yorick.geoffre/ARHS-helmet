#include "hmc5883L.hpp"
#include <math.h>
#include "PinDefinitions.hpp"

#define _x valuesScaled[X]
#define _y valuesScaled[Y]
#define _z valuesScaled[Z]

Hmc5883L::Hmc5883L(int readAddress = 0x1E, int writeAddress = 0x1E){
    _readAddress = readAddress;
    _writeAddress = writeAddress;
    LOG_INFO("Setting up Hmc5883L");
    I2CManager::writeToRegister8bit(0b01110000, 0x00 , writeAddress );        //Configuration Register A
    I2CManager::writeToRegister8bit(0b11100000, 0x01 , writeAddress );        //Configuration Register B
    I2CManager::writeToRegister8bit(0b00000000, 0x02 , writeAddress );        //Mode register continuous measurement mode
    LOG_INFO("Done setting up Hmc5883L");
}

void Hmc5883L::beginUpdateTask(){
    while(true){
        vTaskDelay(100/ portTICK_RATE_MS);
        LOG_VERBOSE("reading raw values");
        readRawValues();
    }
}

void Hmc5883L::beginUpdateTaskWrapper(void *classInstance){
    LOG_INFO("beginUpdateTaskWrapper");
    Hmc5883L *obj = static_cast<Hmc5883L *>(classInstance);
    obj->beginUpdateTask();
}

void Hmc5883L::readRawValues(){
    LOG_VERBOSE("Obtaining raw data");
    Wire.beginTransmission(_readAddress);
    Wire.write(0x03);                  // starting with register 0x03 (Data Output X MSB Register) [HMC5883L datasheet page 11]
    Wire.endTransmission(false);       // the parameter indicates that the Arduino will send a restart. As a result, the connection is kept active.
    Wire.requestFrom(_readAddress, 6, 1); // request a total of 3*2=6 registers, and send the stop message
    LOG_VERBOSE("Obtained raw data");
    for (int i = 0; i < 3; i++)
    {
        values[i] = Wire.read() << 8 | Wire.read();  //we read the two registers at the same time, and concat them to get the full uint16_t (2*8)
        valuesScaled[i] = ((double)values[i]);
    }
    LOG_VERBOSE("Concatenated and scaled the data");
    radius = sqrt(pow(valuesScaled[X],2) + pow(valuesScaled[Y],2) + pow(valuesScaled[Z],2));
    azimuth = atan2(valuesScaled[Y], valuesScaled[X]);
    elevation = atan2(valuesScaled[Z], sqrt(pow(valuesScaled[X], 2) + pow(valuesScaled[Y], 2)));
    heading = atan2(values[Y], values[X]) * 180 / M_PI;
    LOG_VERBOSE("Recovered heading: "+String(heading));
    double head = 0;
    if (_x > 0) {
        heading = 90 - std::atan(_z/_x) * 180/M_PI;
    } else if (_x < 0) {
        heading = 270 - std::atan(_z/_x) * 180/M_PI;
    } else {
    if (_z < 0) {
        heading = 180.0;
    } else {
        heading = 0.0;
    }
  }
}

String Hmc5883L::getDebugValues(){
    return "X:" + String(_x) + "\nY:" +  String(_y) +"\nZ:"+ String(_z);
}