#include "HellMetServo.hpp"
#include "PinDefinitions.hpp"

#define USE_ESP32_TIMER_NO          0
#define TIMER_INTERRUPT_DEBUG       0
#define ISR_SERVO_DEBUG             0
#define MIN_MICROS      800
#define MAX_MICROS      2450
#define ADDITIONNAL_MOVEMENT_OFFSET 25

#include <ESP32_C3_ISR_Servo.h>

HellMetServo::HellMetServo(int origin, int movement, int pin) : _origin(origin), _movement(movement), _pin(pin){ 
    LOG_INFO("Creating servo on pin" + String(pin));
    servoHandle = ESP32_ISR_Servos.setupServo(pin, MIN_MICROS, MAX_MICROS);
    LOG_INFO("Servo Created with handle" + String(servoHandle));
    lastAngle = origin;
    _angle = origin;
}

void HellMetServo::StartToggle(){
    wasClosing ? StartOpen() : StartClose();
}

void HellMetServo::StartClose(){
    _start = _origin-_movement;
    _target = _origin;
    _angle = _start;
    wasClosing = true;
}

void HellMetServo::StartOpen(){
    _start = _origin;
    _target = _origin-_movement;
    _angle = _start;
    wasClosing = false;
}

bool HellMetServo::Step(){
    if(_angle == _target) return true;
    float completion = abs(_target - lastAngle);
    _angle += (_angle > _target) ? -1 : 1;
    lastAngle = _angle;
    if(_angle > 180 || _angle < 0 || !ESP32_ISR_Servos.setPosition(servoHandle, _angle)){
        LOG_ERROR("Error moving servo "+String(_pin)+" to angle "+_angle);
        return true;    //stop
    }
    return _angle == _target;
}

void HellMetServo::Disable(){
    if(!ESP32_ISR_Servos.disable(servoHandle))
        LOG_ERROR("Error disableing servo "+String(_pin));
}

void HellMetServo::Enable(){
    if(!ESP32_ISR_Servos.enable(servoHandle))
        LOG_ERROR("Error enabling servo "+String(_pin));
}

//HellMetServoManager defintions ---------------------------------------------------------------------------------------------------------------------------------------

HellMetServoManager::HellMetServoManager(){
    ESP32_ISR_Servos.useTimer(USE_ESP32_TIMER_NO);

    allServos.push_back(new HellMetServo(90+3+ADDITIONNAL_MOVEMENT_OFFSET, (50+ADDITIONNAL_MOVEMENT_OFFSET), GPIO_NUM_3));  //Top right
    allServos.push_back(new HellMetServo(78+0+6+ADDITIONNAL_MOVEMENT_OFFSET, (50+ADDITIONNAL_MOVEMENT_OFFSET), GPIO_NUM_2));  //bottom right
    allServos.push_back(new HellMetServo(28+4+1-ADDITIONNAL_MOVEMENT_OFFSET, -(50+ADDITIONNAL_MOVEMENT_OFFSET), GPIO_NUM_4)); //bottom left
    allServos.push_back(new HellMetServo(28+13-ADDITIONNAL_MOVEMENT_OFFSET, -(50+ADDITIONNAL_MOVEMENT_OFFSET), GPIO_NUM_5)); //top left
    //vTaskDelay(100);
}

void HellMetServoManager::ToggleAll(){
    LOG_INFO("Beginning Toggle for "+String(allServos.size())+" servos");
    for (HellMetServo* servo : allServos)
    {
        LOG_INFO("Enabling");
        servo->Enable();
        LOG_INFO("Toggling");
        servo->StartToggle();
    }
    LOG_INFO("Done enabling servos");
    bool finished = false;
    int i = 0;
    while(!finished){
        finished = true;
        for(HellMetServo* servo : allServos){
            i++;
            LOG_WARN("not finished for servo "+String(servo->getHandle())+" angle: "+String(servo->getCurrentAngle())+" target: "+String(servo->getCurrentTarget()));
            if(!servo->Step()){
                LOG_INFO("Servo returned false "+String(i));
                finished = false;
                }
                LOG_INFO("Servo didn't return false "+String(i));
        }
        vTaskDelay(10/ portTICK_RATE_MS);
    }
    LOG_INFO("Done moving servos");
    vTaskDelay(100/ portTICK_RATE_MS);

    for (HellMetServo* servo : allServos)
    {
        servo->Disable();
    }
    LOG_INFO("Toggle done");
}

void HellMetServoManager::ToggleAllWrapper(void *classInstance){
        // Get a pointer to the object instance
        HellMetServoManager *obj = static_cast<HellMetServoManager *>(classInstance);
        obj->in_use = true;
        // Call the object method
        obj->ToggleAll();
        obj->in_use = false;
        vTaskDelete(NULL);
}