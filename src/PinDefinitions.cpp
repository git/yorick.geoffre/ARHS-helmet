#include "PinDefinitions.hpp"

void THREAD_SAFE_PRINTLN(String data)
{

    // Wait for the mutex to be available
    xSemaphoreTake(serial_mutex, 10);

    // Use the Serial class to send and receive data
    Serial.println(data);

    // Release the mutex
    xSemaphoreGive(serial_mutex);
}